#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>
#include "ticket/SingleTicket.h"
#include "ticket/MonthlyTicket.h"
#include "managers/TicketsManager.h"
#include "managers/TrainsManager.h"

using namespace boost::local_time;
using namespace boost::posix_time;
using namespace boost::gregorian;
using namespace std;

int main()
{
	ptime pt1(date(2020,Jan,6), time_duration(22,20,0));
	ptime pt2(date(2020,Jan,7),time_duration(2,30,0));
	time_zone_ptr zone(new posix_time_zone("UTC+01"));
	local_date_time ldt1(pt1, zone);
	local_date_time ldt2(pt2, zone);

	string train1ID = "TLK 19203";
	string train2ID = "LKA 21930";
	string train3ID = "IC 11233";

	TrainsManager trainsManager;
	TicketsManager ticketsManager;

	TrainPtr train1(new Train(train1ID, 500, "Krakow Glowny", "Bialystok Glowny", TrainType(TrainType::Type::REGULAR)));
	TrainPtr train2(new Train(train2ID, 50, "Kutno", "Lodz Widzew", TrainType(TrainType::Type::REGIONAL)));
	TrainPtr train3(new Train(train3ID, 300, "Krakow Glowny", "Warszawa Wschodnia",
							  TrainType(TrainType::Type::HIGH_SPEED)));

	trainsManager.addTrain(train1);
	trainsManager.addTrain(train2);
	trainsManager.addTrain(train3);

	TicketPtr singleTicket1(new SingleTicket(false, trainsManager.getTrainByID(train1ID), Ticket::PaymentMethod::CASH, ldt1, ldt2));
	TicketPtr singleTicket2(new SingleTicket(true, trainsManager.getTrainByID(train3ID), Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
	TicketPtr monthlyTicket(new MonthlyTicket(false, trainsManager.getTrainByID(train2ID), Ticket::PaymentMethod::CASH, ldt2, Passenger(PassengerType::PENSIONER)));

	ticketsManager.addTicket(singleTicket1);
	ticketsManager.addTicket(singleTicket2);
	ticketsManager.addTicket(monthlyTicket);

	cout << "Trains:\n";
	cout << trainsManager.getTrainByID(train1ID)->toString() << '\n';
	cout << trainsManager.getTrainByID(train2ID)->toString() << '\n';
	cout << trainsManager.getTrainByID(train3ID)->toString() << '\n';

	vector<const Ticket*> tickets;

	cout << "\nTickets:\n";
	ticketsManager.getTicketsByTrain(trainsManager.getTrainByID(train1ID), tickets);
	cout << tickets[0]->toString() << '\n';
	ticketsManager.getTicketsByTrain(trainsManager.getTrainByID(train2ID), tickets);
	cout << tickets[0]->toString() << '\n';
	ticketsManager.getTicketsByTrain(trainsManager.getTrainByID(train3ID), tickets);
	cout << tickets[0]->toString() << '\n';

	return 0;
}