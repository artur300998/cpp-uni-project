cmake_minimum_required(VERSION 3.4)

project(program)

set(SOURCE_FILES
        src/main.cpp
        )

add_executable(program ${SOURCE_FILES})
target_link_libraries(program library)

target_include_directories(program PUBLIC include)