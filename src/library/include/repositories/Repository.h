//
// Created by student on 03.01.2020.
//

#ifndef TRAINTICKETS_REPOSITORY_H
#define TRAINTICKETS_REPOSITORY_H

#include <vector>
#include "functors/SearchFunc.h"

template<class T>
class Repository
{
public:
    Repository() {};
    virtual ~Repository() {};
    virtual void create(T& t) = 0;
    virtual void remove(const T& t) = 0;
    virtual const std::vector<T>* getAll() const = 0;
    virtual void find(SearchFunc<T>& searchFunc) = 0;
};

#endif //TRAINTICKETS_REPOSITORY_H
