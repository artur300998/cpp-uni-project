//
// Created by student on 03.01.2020.
//

#ifndef TRAINTICKETS_TRAINSREPOSITORY_H
#define TRAINTICKETS_TRAINSREPOSITORY_H

#include "Repository.h"
#include "train/Train.h"
#include "functors/SearchFunc.h"


class TrainsRepository : public Repository<TrainPtr>
{
private:
	std::vector<TrainPtr> trains;

public:
	TrainsRepository();
    TrainsRepository(const std::vector<TrainPtr>& trains);

    virtual ~TrainsRepository();
    void create(TrainPtr& train);
    void remove(const TrainPtr& train);
    const std::vector<TrainPtr>* getAll() const;
    void find(SearchFunc<TrainPtr>& searchFunc);
};

#endif //TRAINTICKETS_TRAINSREPOSITORY_H
