//
// Created by student on 03.01.2020.
//

#ifndef TRAINTICKETS_TICKETSREPOSITORY_H
#define TRAINTICKETS_TICKETSREPOSITORY_H

#include <memory>
#include "ticket/Ticket.h"
#include "Repository.h"
#include "functors/SearchFunc.h"


class TicketsRepository : public Repository<TicketPtr>
{
private:
	std::vector<TicketPtr> tickets;

public:
	TicketsRepository();
    TicketsRepository(const std::vector<TicketPtr>& tickets);

    virtual ~TicketsRepository();
    void create(TicketPtr& ticket);
    void remove(const TicketPtr& ticket);
    const std::vector<TicketPtr>* getAll() const;
    void find(SearchFunc<TicketPtr>& searchFunc);
};

#endif //TRAINTICKETS_TICKETSREPOSITORY_H
