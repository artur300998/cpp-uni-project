#ifndef TRAINTICKETS_SEARCHFUNC_H
#define TRAINTICKETS_SEARCHFUNC_H


template <typename T>
class SearchFunc
{
public:
	SearchFunc() {};
	virtual ~SearchFunc() {};
	virtual bool operator()(const T& arg) = 0;
	virtual void addFound(const void* found) = 0;
	virtual int foundAmount() = 0;
};

#endif //TRAINTICKETS_SEARCHFUNC_H
