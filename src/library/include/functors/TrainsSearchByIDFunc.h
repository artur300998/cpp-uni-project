#ifndef TRAINSSEARCHBYIDFUNC_H
#define TRAINSSEARCHBYIDFUNC_H

#include "train/Train.h"
#include "SearchFunc.h"

class TrainsSearchByIDFunc : public SearchFunc<TrainPtr>
{
	std::string IDToSearchFor;
	const Train* foundTrain;

public:
	TrainsSearchByIDFunc(const std::string& IDToSearchFor);
	~TrainsSearchByIDFunc();

	bool operator()(const TrainPtr& train);
	void addFound(const void* train);
	int foundAmount();
	const Train* getFoundTrain();
};


#endif //TRAINSSEARCHBYIDFUNC_H
