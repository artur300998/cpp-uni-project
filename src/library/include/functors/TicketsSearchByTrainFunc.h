#ifndef TICKETSSEARCHBYTRAINFUNC_H
#define TICKETSSEARCHBYTRAINFUNC_H

#include "ticket/Ticket.h"
#include "train/Train.h"
#include "SearchFunc.h"

class TicketsSearchByTrainFunc : public SearchFunc<TicketPtr>
{
	const Train* trainToSearchFor;
	std::vector<const Ticket*> foundTickets;

public:
	TicketsSearchByTrainFunc(const Train* trainToSearchFor);
	~TicketsSearchByTrainFunc();

	bool operator()(const TicketPtr& ticket);
	void addFound(const void* ticket);
	int foundAmount();
	const std::vector<const Ticket*>& getFoundTickets();
};


#endif //TICKETSSEARCHBYTRAINFUNC_H
