//
// Created by student on 29.12.2019.
//

#ifndef TRAINTICKETS_MONTHLYTICKET_H
#define TRAINTICKETS_MONTHLYTICKET_H

#include "Ticket.h"
#include "Passenger.h"


class MonthlyTicket : public Ticket
{
private:
    boost::local_time::local_date_time validFrom;
    Passenger passenger;

public:
    MonthlyTicket(bool isFirstClassWagon, const Train* train, PaymentMethod paymentMethod,
				  const boost::local_time::local_date_time& validFrom, Passenger passenger);

    virtual ~MonthlyTicket();
	void addPassenger(Passenger passenger);
    const boost::local_time::local_date_time& getValidFrom() const;
	unsigned int getNumberOfPassengers() const;
    float calculateCost() const;
    std::string toString() const;
};

#endif //TRAINTICKETS_MONTHLYTICKET_H
