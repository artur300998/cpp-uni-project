//
// Created by student on 29.12.2019.
//

#ifndef TRAINTICKETS_TICKET_H
#define TRAINTICKETS_TICKET_H

#include <boost/date_time/local_time/local_time.hpp>
#include <boost/uuid/uuid.hpp>
#include <memory>
#include <iostream>
#include "Passenger.h"

class Train;

class Ticket
{
public:
	enum PaymentMethod {ONLINE, CASH};
private:
    boost::uuids::uuid ID;
    bool isFirstClassWagon;
    const Train* train;
    PaymentMethod paymentMethod;

public:
	Ticket(bool isFirstClassWagon, const Train* train, PaymentMethod paymentMethod);
	virtual ~Ticket();

    bool getIsFirstClassWagon() const;
    const boost::uuids::uuid& getId() const;
    const Train* getTrain() const;
    PaymentMethod getPaymentMethod() const;
	virtual unsigned int getNumberOfPassengers() const = 0;
	virtual void addPassenger(Passenger passenger) = 0;
    virtual float calculateCost() const;
    virtual std::string toString() const;
	bool operator==(const Ticket& ticket) const;
};

typedef std::shared_ptr<Ticket> TicketPtr;

#endif //TRAINTICKETS_TICKET_H
