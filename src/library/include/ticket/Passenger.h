//
// Created by student on 29.12.2019.
//

#ifndef TRAINTICKETS_PASSENGER_H
#define TRAINTICKETS_PASSENGER_H

enum PassengerType {STUDENT, CHILD, PENSIONER, REGULAR, VETERAN};

class Passenger
{
private:
    PassengerType passengerType;

public:
    Passenger(PassengerType passengerType);
    virtual ~Passenger();

    PassengerType getPassengerType() const;
    float getDiscount();
};
#endif //TRAINTICKETS_PASSENGER_H
