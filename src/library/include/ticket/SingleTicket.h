//
// Created by student on 29.12.2019.
//

#ifndef TRAINTICKETS_SINGLETICKET_H
#define TRAINTICKETS_SINGLETICKET_H

#include "Ticket.h"
#include <vector>

class SingleTicket : public Ticket
{
private:
    boost::local_time::local_date_time departureTime;
    boost::local_time::local_date_time arrivalTime;
    std::vector<Passenger> passengers;

public:
    SingleTicket(bool isFirstClassWagon, const Train* train, PaymentMethod paymentMethod,
				 const boost::local_time::local_date_time& departureTime,
				 const boost::local_time::local_date_time& arrivalTime);

    virtual ~SingleTicket();
    void addPassenger(Passenger passenger);
    const boost::local_time::local_date_time& getDepartureTime() const;
    const boost::local_time::local_date_time& getArrivalTime() const;
	unsigned int getNumberOfPassengers() const;
    float calculateCost() const;
    std::string toString() const;
};

#endif //TRAINTICKETS_SINGLETICKET_H
