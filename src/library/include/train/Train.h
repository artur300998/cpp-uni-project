//
// Created by student on 29.12.2019.
//

#ifndef TRAINTICKETS_TRAIN_H
#define TRAINTICKETS_TRAIN_H


#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <string>
#include <memory>
#include "TrainType.h"

class Train
{
private:
    std::string ID;
    unsigned int distance;
    std::string departureStation;
    std::string arrivalStation;
    TrainType trainType;
    unsigned int capacity;
	unsigned int occupiedSeats;

public:

	Train(const std::string& id, unsigned int distance, const std::string& departureStation,
		  const std::string& arrivalStation, TrainType trainType, unsigned int capacity = 200);
	virtual ~Train();

    const std::string &getId() const;
	unsigned int getDistance() const;
	const std::string &getDepartureStation() const;
	const std::string &getArrivalStation() const;
	const TrainType& getTrainType() const;
	unsigned int getCapacity() const;
	unsigned int getOccupiedSeats() const;
	float distanceCost() const;

	void occupySeats(int amount);
	void freeSeats(int amount);

    std::string toString() const;
    bool operator==(const Train& train) const;
};

typedef std::shared_ptr<Train> TrainPtr;

#endif //TRAINTICKETS_TRAIN_H
