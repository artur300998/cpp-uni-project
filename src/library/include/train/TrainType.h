//
// Created by student on 29.12.2019.
//

#ifndef TRAINTICKETS_TRAINTYPE_H
#define TRAINTICKETS_TRAINTYPE_H

#include <string>

class TrainType
{
public:
	enum Type {REGULAR, HIGH_SPEED, REGIONAL};

private:
	Type type;
	float (TrainType::*distanceCostFun)(unsigned int) const;

	float regularTrainDistanceCost(unsigned int distance) const;
	float highSpeedTrainDistanceCost(unsigned int distance) const;
	float regionalTrainDistanceCost(unsigned int distance) const;

public:
	TrainType(Type type);
	~TrainType();

	float distanceCost(unsigned int distance) const;
	std::string toString() const;
};

#endif //TRAINTICKETS_TRAINTYPE_H
