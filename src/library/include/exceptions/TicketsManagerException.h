//
// Created by student on 05.01.2020.
//

#ifndef TRAINTICKETS_TICKETSMANAGEREXCEPTION_H
#define TRAINTICKETS_TICKETSMANAGEREXCEPTION_H

#include <stdexcept>
#include <iostream>

class TicketsManagerException : public std::logic_error
{
public:
    TicketsManagerException() : logic_error("Tickets manager exception: ") {}
    const char* ticketAlreadyExists = "Ticket already exists";
    const char* ticketDoesntExist = "Ticket doesn't exist";
};

#endif //TRAINTICKETS_TICKETSMANAGEREXCEPTION_H
