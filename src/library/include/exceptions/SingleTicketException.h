//
// Created by student on 05.01.2020.
//

#ifndef TRAINTICKETS_SINGLETICKETEXCEPTION_H
#define TRAINTICKETS_SINGLETICKETEXCEPTION_H

#include <stdexcept>
#include <iostream>

class SingleTicketException : public std::logic_error
{
public:
    SingleTicketException() : logic_error("Single ticket exception: ") {}
    const char* arrivalTimeLessThanDepartureTime = "Arrival time less than departure time";
};

#endif //TRAINTICKETS_SINGLETICKETEXCEPTION_H
