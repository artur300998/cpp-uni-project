//
// Created by student on 05.01.2020.
//

#ifndef TRAINTICKETS_TRAINSMANAGEREXCEPTION_H
#define TRAINTICKETS_TRAINSMANAGEREXCEPTION_H

#include <stdexcept>
#include <iostream>

class TrainsManagerException : public std::logic_error
{
public:
    TrainsManagerException() : logic_error("Trains manager exception: ") {}
    const char* trainAlreadyExists = "Train already exists";
    const char* trainDoesntExist = "Train doesn't exist";
};

#endif //TRAINTICKETS_TRAINSMANAGEREXCEPTION_H
