//
// Created by student on 05.01.2020.
//

#ifndef TRAINTICKETS_TRAINEXCEPTION_H
#define TRAINTICKETS_TRAINEXCEPTION_H

#include <stdexcept>
#include <iostream>

class TrainException : public std::logic_error
{
public:
    TrainException() : logic_error("Train exception: ") {}
    const char* distanceIsZero = "Distance is zero";
    const char* IDIsEmpty = "ID is empty";
    const char* trainIsNullptr = "Train is nullptr";
    const char* noSeatsAvailable = "There are not that many seats available.";
    const char* cantFreeSeats = "There are not enough occupied seats to free.";
};

#endif //TRAINTICKETS_TRAINEXCEPTION_H
