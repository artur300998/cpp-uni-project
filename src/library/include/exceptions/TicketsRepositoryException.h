//
// Created by student on 05.01.2020.
//

#ifndef TRAINTICKETS_TICKETSREPOSITORYEXCEPTION_H
#define TRAINTICKETS_TICKETSREPOSITORYEXCEPTION_H

#include <stdexcept>
#include <iostream>

class TicketsRepositoryException : public std::logic_error
{
public:
    TicketsRepositoryException() : logic_error("Tickets repository exception: ") {}
    const char* ticketNotFoundOnRemove = "Ticket not found on remove";
};

#endif //TRAINTICKETS_TICKETSREPOSITORYEXCEPTION_H
