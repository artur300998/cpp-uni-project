//
// Created by student on 05.01.2020.
//

#ifndef TRAINTICKETS_TICKETEXCEPTION_H
#define TRAINTICKETS_TICKETEXCEPTION_H

#include <stdexcept>
#include <iostream>

class TicketException : public std::logic_error
{
public:
    TicketException() : logic_error("Ticket exception: ") {}
    const char* ticketIsNullptr = "Ticket is nullptr";
};

#endif //TRAINTICKETS_TICKETEXCEPTION_H
