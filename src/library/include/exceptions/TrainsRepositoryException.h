//
// Created by student on 05.01.2020.
//

#ifndef TRAINTICKETS_TRAINSREPOSITORYEXCEPTION_H
#define TRAINTICKETS_TRAINSREPOSITORYEXCEPTION_H

#include <stdexcept>
#include <iostream>

class TrainsRepositoryException : public std::logic_error
{
public:
    TrainsRepositoryException() : logic_error("Trains repository exception: ") {}
    const char* trainAlreadyExists = "Train already exists";
    const char* trainNotFoundOnRemove = "Train not found on remove";
};

#endif //TRAINTICKETS_TRAINSREPOSITORYEXCEPTION_H
