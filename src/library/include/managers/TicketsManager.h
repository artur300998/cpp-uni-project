//
// Created by student on 03.01.2020.
//

#ifndef TRAINTICKETS_TICKETSMANAGER_H
#define TRAINTICKETS_TICKETSMANAGER_H

#include "repositories/TicketsRepository.h"

class TicketsManager
{
private:
    TicketsRepository ticketsRepository;

public:
    TicketsManager();
    virtual ~TicketsManager();
    void addTicket(TicketPtr& ticket);
    void removeTicket(const TicketPtr& ticket);
    int getTicketsByTrain(const Train* train, std::vector<const Ticket*>& tickets);
};

#endif //TRAINTICKETS_TICKETSMANAGER_H
