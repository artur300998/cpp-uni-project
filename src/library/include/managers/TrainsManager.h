//
// Created by student on 03.01.2020.
//

#ifndef TRAINTICKETS_TRAINSMANAGER_H
#define TRAINTICKETS_TRAINSMANAGER_H

#include "repositories/TrainsRepository.h"

class TrainsManager
{
private:
    TrainsRepository trainsRepository;

public:
    TrainsManager();
    virtual ~TrainsManager();
    void addTrain(TrainPtr& train);
    void removeTrain(const TrainPtr& train);
    const Train* getTrainByID(const std::string& id);
};

#endif //TRAINTICKETS_TRAINSMANAGER_H
