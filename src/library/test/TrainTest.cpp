#include <boost/test/unit_test.hpp>
#include "train/Train.h"
#include "exceptions/TrainException.h"

BOOST_AUTO_TEST_SUITE(TestSuiteCorrect)

	BOOST_AUTO_TEST_CASE(DistanceCostCase)
	{
		Train train1("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL));
		Train train2("IC 10033", 300, "Krakow", "Warszawa", TrainType(TrainType::Type::HIGH_SPEED));
		Train train3("TLK 91003", 200, "Wroclaw", "Poznan", TrainType(TrainType::Type::REGULAR));

		BOOST_REQUIRE_CLOSE(train1.distanceCost(), 45, 0.01);
		BOOST_REQUIRE_CLOSE(train2.distanceCost(), 184.85f, 0.01);
		BOOST_REQUIRE_CLOSE(train3.distanceCost(), 70, 0.01);
	}

    BOOST_AUTO_TEST_CASE(DistanceErrorCase)
    {
        try
        {
            Train train("LKA 19992", 0, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL));
        }
        catch(const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, TrainException().distanceIsZero);
        }
    }

    BOOST_AUTO_TEST_CASE(IDErrorCase)
    {
        try
        {
            Train train("", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL));
        }
        catch (const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, TrainException().IDIsEmpty);
        }
    }

    BOOST_AUTO_TEST_CASE(NoAvailableSeatsCase)
	{
		Train train1("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL), 200);

		try
		{
			train1.occupySeats(201);
		}
		catch (const char* msg)
		{
			BOOST_REQUIRE_EQUAL(msg, TrainException().noSeatsAvailable);
		}
	}

	BOOST_AUTO_TEST_CASE(CantFreeSeatsCase)
	{
		Train train1("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL), 200);
		train1.occupySeats(10);
		try
		{
			train1.freeSeats(11);
		}
		catch (const char* msg)
		{
			BOOST_REQUIRE_EQUAL(msg, TrainException().cantFreeSeats);
		}
	}

BOOST_AUTO_TEST_SUITE_END()