#include <boost/test/unit_test.hpp>
#include <train/Train.h>
#include <functors/TicketsSearchByTrainFunc.h>
#include <exceptions/TicketException.h>
#include "exceptions/TrainException.h"

BOOST_AUTO_TEST_SUITE(TestSuiteCorrect)

	BOOST_AUTO_TEST_CASE(TicketSearchByTrainFuncExceptionsCase)
	{
		Train train("TLK 91003", 200, "Wroclaw", "Poznan", TrainType(TrainType::Type::REGULAR));

		try
		{
			TicketsSearchByTrainFunc ticketsSearch(nullptr);
		}
		catch (const char* msg)
		{
			BOOST_REQUIRE_EQUAL(msg, TrainException().trainIsNullptr);
		}

		TicketsSearchByTrainFunc ticketsSearch(&train);
		try
		{
			ticketsSearch.addFound(nullptr);
		}
		catch (const char* msg)
		{
			BOOST_REQUIRE_EQUAL(msg, TicketException().ticketIsNullptr);
		}

	}

BOOST_AUTO_TEST_SUITE_END()