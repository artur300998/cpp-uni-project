#include <boost/test/unit_test.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>
#include "ticket/MonthlyTicket.h"
#include "ticket/SingleTicket.h"
#include "repositories/TicketsRepository.h"
#include "repositories/TrainsRepository.h"
#include "functors/TicketsSearchByTrainFunc.h"
#include "functors/TrainsSearchByIDFunc.h"
#include "exceptions/TrainsRepositoryException.h"
#include "exceptions/TicketsRepositoryException.h"

using namespace boost::local_time;
using namespace boost::posix_time;
using namespace boost::gregorian;
using namespace std;

BOOST_AUTO_TEST_SUITE(TestSuiteCorrect)

    BOOST_AUTO_TEST_CASE(TrainsRepositoryNoTrainOnRemoveCase)
    {
        TrainsRepository trainsRepository;

        TrainPtr train (new Train("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL)));
        try
        {
            trainsRepository.remove(train);
        } catch (const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, TrainsRepositoryException().trainNotFoundOnRemove);
        }

    }

    BOOST_AUTO_TEST_CASE(TrainsRepositoryCreateRemoveTrainCase)
    {
        TrainsRepository trainsRepository;

        TrainPtr train1 (new Train("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL)));
        TrainPtr train2 (new Train("IC 10033", 300, "Krakow", "Warszawa", TrainType(TrainType::Type::HIGH_SPEED)));
        TrainPtr train3 (new Train("TLK 91003", 200, "Wroclaw", "Poznan", TrainType(TrainType::Type::REGULAR)));

        trainsRepository.create(train1);
        trainsRepository.create(train2);
        trainsRepository.create(train3);

        vector<TrainPtr> trains;
        trains.push_back(train1);
        trains.push_back(train2);
        trains.push_back(train3);

		const vector<TrainPtr>* repositoryTrains = trainsRepository.getAll();
		for(vector<TrainPtr>::size_type i = 0; i < repositoryTrains->size(); i++)
		{
			BOOST_REQUIRE(*((*repositoryTrains)[i]) == *(trains[i]));
		}

        trainsRepository.remove(train1);
        trains.erase(trains.begin());

		repositoryTrains = trainsRepository.getAll();
		for(vector<TrainPtr>::size_type i = 0; i < repositoryTrains->size(); i++)
		{
			BOOST_REQUIRE(*((*repositoryTrains)[i]) == *(trains[i]));
		}
    }
/*    BOOST_AUTO_TEST_CASE(TrainsRepositoryFindTrainCase)
    {
        TrainsRepository trainsRepository;

		TrainPtr train1 (new Train("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL)));
		TrainPtr train2 (new Train("IC 10033", 300, "Krakow", "Warszawa", TrainType(TrainType::Type::HIGH_SPEED)));
		TrainPtr train3 (new Train("TLK 91003", 200, "Wroclaw", "Poznan", TrainType(TrainType::Type::REGULAR)));
        trainsRepository.create(train1);
        trainsRepository.create(train2);
        trainsRepository.create(train3);


    }*/

	BOOST_AUTO_TEST_CASE(TicketsRepositoryFindTicketsCase)
	{
		TicketsRepository ticketsRepository;

		Train train1("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL));
		Train train2("IC 10033", 300, "Krakow", "Warszawa", TrainType(TrainType::Type::HIGH_SPEED));
		Train train3("TLK 91003", 200, "Wroclaw", "Poznan", TrainType(TrainType::Type::REGULAR));

		ptime pt1(date(2020,Jan,6), time_duration(22,20,0));
		ptime pt2(date(2020,Jan,7),time_duration(2,30,0));
		time_zone_ptr zone(new posix_time_zone("UTC+01"));
		local_date_time ldt1(pt1, zone);
		local_date_time ldt2(pt2, zone);

		TicketPtr singleTicket1 (new SingleTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
		TicketPtr singleTicket2 (new SingleTicket(false, &train2, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
		TicketPtr monthlyTicket (new MonthlyTicket(false, &train3, Ticket::PaymentMethod::ONLINE, ldt1, Passenger(PassengerType::PENSIONER)));

		ticketsRepository.create(singleTicket1);
		ticketsRepository.create(singleTicket2);
		ticketsRepository.create(monthlyTicket);

		TicketsSearchByTrainFunc ticketsSearchByTrainFunc(&train1);
		ticketsRepository.find(ticketsSearchByTrainFunc);

		BOOST_REQUIRE_EQUAL(ticketsSearchByTrainFunc.foundAmount(), 1);
		BOOST_REQUIRE_EQUAL(singleTicket1.get(), ticketsSearchByTrainFunc.getFoundTickets()[0]);
	}

    BOOST_AUTO_TEST_CASE(TicketsRepositoryCreateRemoveTicketCase)
    {
        TicketsRepository ticketsRepository;

        Train train1("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL));
        Train train2("IC 10033", 300, "Krakow", "Warszawa", TrainType(TrainType::Type::HIGH_SPEED));
        Train train3("TLK 91003", 200, "Wroclaw", "Poznan", TrainType(TrainType::Type::REGULAR));

        ptime pt1(date(2020,Jan,6), time_duration(22,20,0));
        ptime pt2(date(2020,Jan,7),time_duration(2,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        TicketPtr singleTicket1 (new SingleTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
        TicketPtr singleTicket2 (new SingleTicket(false, &train2, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
        TicketPtr monthlyTicket (new MonthlyTicket(false, &train3, Ticket::PaymentMethod::ONLINE, ldt1, Passenger(PassengerType::PENSIONER)));

        ticketsRepository.create(singleTicket1);
        ticketsRepository.create(singleTicket2);
        ticketsRepository.create(monthlyTicket);

        vector<TicketPtr> tickets;
        tickets.push_back(singleTicket1);
        tickets.push_back(singleTicket2);
        tickets.push_back(monthlyTicket);

		const vector<TicketPtr>* repositoryTickets = ticketsRepository.getAll();
		for(vector<TicketPtr>::size_type i = 0; i < repositoryTickets->size(); i++)
		{
			BOOST_REQUIRE(*((*repositoryTickets)[i]) == *(tickets[i]));
		}

        ticketsRepository.remove(singleTicket1);
        tickets.erase(tickets.begin());

		repositoryTickets = ticketsRepository.getAll();
		for(vector<TicketPtr>::size_type i = 0; i < repositoryTickets->size(); i++)
		{
			BOOST_REQUIRE(*((*repositoryTickets)[i]) == *(tickets[i]));
		}
    }

    BOOST_AUTO_TEST_CASE(TicketsRepositoryNoTicketOnRemoveCase)
    {
        TicketsRepository ticketsRepository;

        Train train("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL));

        ptime pt1(date(2020,Jan,6), time_duration(22,20,0));
        ptime pt2(date(2020,Jan,7),time_duration(2,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        TicketPtr singleTicket (new SingleTicket(false, &train, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
        try
        {
            ticketsRepository.remove(singleTicket);
        } catch (const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, TicketsRepositoryException().ticketNotFoundOnRemove);
        }
    }

BOOST_AUTO_TEST_SUITE_END()