#include <boost/test/unit_test.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>
#include "ticket/MonthlyTicket.h"
#include "ticket/SingleTicket.h"
#include "managers/TicketsManager.h"
#include "managers/TrainsManager.h"
#include "exceptions/TrainsManagerException.h"
#include "exceptions/TicketsManagerException.h"
#include "exceptions/TrainException.h"

using namespace boost::local_time;
using namespace boost::posix_time;
using namespace boost::gregorian;
using namespace std;

BOOST_AUTO_TEST_SUITE(TestSuiteCorrect)

    BOOST_AUTO_TEST_CASE(TrainsManagerAddTrainCase)
    {
        TrainsManager trainsManager;
        string id1 = "IC 10133";
        string id2 = "LKA 19992";
        string id3 = "TLK 91003";

        TrainPtr train1(new Train(id1, 500, "Gdansk", "Warszawa", TrainType(TrainType::Type::REGULAR)));
        TrainPtr train2(new Train(id2, 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL)));
        TrainPtr train3(new Train(id3, 200, "Wroclaw", "Poznan", TrainType(TrainType::Type::REGULAR)));

        trainsManager.addTrain(train1);
        trainsManager.addTrain(train2);
        trainsManager.addTrain(train3);

        BOOST_REQUIRE(*trainsManager.getTrainByID(id1) == *train1);
        BOOST_REQUIRE(*trainsManager.getTrainByID(id2) == *train2);
        BOOST_REQUIRE(*trainsManager.getTrainByID(id3) == *train3);
    }
    BOOST_AUTO_TEST_CASE(TrainsManagerAddRemoveTrainCase)
    {
        TrainsManager trainsManager;
        string id = "IC 10133";

        TrainPtr train(new Train(id, 500, "Gdansk", "Warszawa", TrainType(TrainType::Type::REGULAR)));
        trainsManager.addTrain(train);

        BOOST_REQUIRE(*trainsManager.getTrainByID(id) == *train);

        trainsManager.removeTrain(train);
        try
        {
            trainsManager.getTrainByID(id);
        } catch (const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, TrainsManagerException().trainDoesntExist);
        }
    }

    BOOST_AUTO_TEST_CASE(TrainsManagerRepeatedTrainCase)
    {
        TrainsManager trainsManager;
        string id = "IC 10133";

        TrainPtr train(new Train(id, 500, "Gdansk", "Warszawa", TrainType(TrainType::Type::REGULAR)));
        trainsManager.addTrain(train);

        try
        {
            trainsManager.addTrain(train);
        } catch (const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, TrainsManagerException().trainAlreadyExists);
        }
    }

    BOOST_AUTO_TEST_CASE(TicketsManagerAddTicketCase)
    {
        TicketsManager ticketsManager;
        Train train1("IC 10133", 500, "Gdansk", "Warszawa", TrainType(TrainType::Type::REGULAR));

        ptime pt1(date(2020,Jan,6), time_duration(22,20,0));
        ptime pt2(date(2020,Jan,7),time_duration(2,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        vector<const Ticket*> tickets;

        BOOST_REQUIRE_EQUAL(ticketsManager.getTicketsByTrain(&train1, tickets), 0);

        TicketPtr singleTicket1(new SingleTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
		TicketPtr singleTicket2(new SingleTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
		TicketPtr monthlyTicket(new MonthlyTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, Passenger(PassengerType::PENSIONER)));
        ticketsManager.addTicket(singleTicket1);
        ticketsManager.addTicket(singleTicket2);
        ticketsManager.addTicket(monthlyTicket);

        BOOST_REQUIRE_EQUAL(ticketsManager.getTicketsByTrain(&train1, tickets), 3);
    }

    BOOST_AUTO_TEST_CASE(TicketsManagerAddRemoveTicketCase)
    {
        TicketsManager ticketsManager;
        Train train1("IC 10133", 500, "Gdansk", "Warszawa", TrainType(TrainType::Type::REGULAR));

        ptime pt1(date(2020, Jan, 6), time_duration(22, 20, 0));
        ptime pt2(date(2020, Jan, 7), time_duration(2, 30, 0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        TicketPtr singleTicket1(new SingleTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
        ticketsManager.addTicket(singleTicket1);

        vector<const Ticket*> tickets;

        BOOST_REQUIRE_EQUAL(ticketsManager.getTicketsByTrain(&train1, tickets), 1);

        try {
			ticketsManager.removeTicket(singleTicket1);
        }
        catch (const char* msg) {
            BOOST_REQUIRE_EQUAL(msg, TicketsManagerException().ticketDoesntExist);
        }
    }

    BOOST_AUTO_TEST_CASE(TrainSeatsOccupingTestCase)
	{
		TicketsManager ticketsManager;
		Train train1("IC 10133", 500, "Gdansk", "Warszawa", TrainType(TrainType::Type::REGULAR), 10);

		ptime pt1(date(2020, Jan, 6), time_duration(22, 20, 0));
		ptime pt2(date(2020, Jan, 7), time_duration(2, 30, 0));
		time_zone_ptr zone(new posix_time_zone("UTC+01"));
		local_date_time ldt1(pt1, zone);
		local_date_time ldt2(pt2, zone);

		TicketPtr singleTicket1(new SingleTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, ldt2));
		TicketPtr singleTicket2(new SingleTicket(true, &train1, Ticket::PaymentMethod::CASH, ldt1, ldt2));
		TicketPtr monthlyTicket1(new MonthlyTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, Passenger(PassengerType::PENSIONER)));
		TicketPtr monthlyTicket2(new MonthlyTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt1, Passenger(PassengerType::STUDENT)));

		singleTicket1->addPassenger(PassengerType::VETERAN);
		singleTicket1->addPassenger(PassengerType::REGULAR);
		singleTicket1->addPassenger(PassengerType::REGULAR);
		singleTicket1->addPassenger(PassengerType::REGULAR);
		singleTicket1->addPassenger(PassengerType::STUDENT);
		singleTicket2->addPassenger(PassengerType::CHILD);
		singleTicket2->addPassenger(PassengerType::REGULAR);
		singleTicket2->addPassenger(PassengerType::REGULAR);
		singleTicket2->addPassenger(PassengerType::REGULAR);

		ticketsManager.addTicket(singleTicket1);
		ticketsManager.addTicket(monthlyTicket1);
		ticketsManager.addTicket(monthlyTicket2);

		try
		{
			ticketsManager.addTicket(singleTicket2);
		}
		catch (const char* msg)
		{
			BOOST_REQUIRE_EQUAL(TrainException().noSeatsAvailable, msg);
		}

		ticketsManager.removeTicket(singleTicket1);

		try
		{
			ticketsManager.addTicket(singleTicket2);
		}
		catch (const char* msg)
		{
			BOOST_ERROR(msg);
		}

	}

BOOST_AUTO_TEST_SUITE_END()