#include <boost/test/unit_test.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian_types.hpp>
#include "ticket/SingleTicket.h"
#include "ticket/MonthlyTicket.h"
#include "train/Train.h"

using namespace boost::local_time;
using namespace boost::posix_time;
using namespace boost::gregorian;

BOOST_AUTO_TEST_SUITE(TestSuiteCorrect)

	BOOST_AUTO_TEST_CASE(MonthlyTicketCostCase1)
	{
		Train train1("LKA 19992", 80, "Lodz", "Lowicz", TrainType(TrainType::Type::REGIONAL));

		ptime pt(date(2020,Jan,4), hours(10));
		time_zone_ptr zone(new posix_time_zone("UTC+01"));
		local_date_time ldt(pt, zone);

		MonthlyTicket monthlyTicket(false, &train1, Ticket::PaymentMethod::CASH, ldt, Passenger(PassengerType::REGULAR));

		BOOST_REQUIRE_CLOSE(monthlyTicket.calculateCost(), 450, 0.01);
	}

    BOOST_AUTO_TEST_CASE(MonthlyTicketCostCase2)
    {
        Train train1("LKA 19993", 130, "Lodz", "Katowice", TrainType(TrainType::Type::REGULAR));

        ptime pt(date(2020,May,1), hours(11));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt(pt, zone);

        MonthlyTicket monthlyTicket(false, &train1, Ticket::PaymentMethod::ONLINE, ldt, Passenger(PassengerType::STUDENT));

        BOOST_REQUIRE_CLOSE(monthlyTicket.calculateCost(), 469, 0.01);
    }

    BOOST_AUTO_TEST_CASE(MonthlyTicketCostCase3)
    {
        Train train1("LKA 19994", 200, "Katowice", "Lowicz", TrainType(TrainType::Type::HIGH_SPEED));

        ptime pt(date(2050,Jan,4), hours(12));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt(pt, zone);

        MonthlyTicket monthlyTicket(true, &train1, Ticket::PaymentMethod::CASH, ldt, Passenger(PassengerType::PENSIONER));

        BOOST_REQUIRE_CLOSE(monthlyTicket.calculateCost(), 2673.91, 0.01);
    }


	BOOST_AUTO_TEST_CASE(SingleTicketCostCase1)
    {
        Train train("IC 10033", 300, "Krakow", "Warszawa", TrainType(TrainType::Type::HIGH_SPEED));

        ptime pt1(date(2020,Jan,4), time_duration(10,20,0));
        ptime pt2(date(2020,Jan,4),time_duration(12,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        SingleTicket singleTicket(true, &train, Ticket::PaymentMethod::ONLINE, ldt1, ldt2);
        singleTicket.addPassenger(Passenger(STUDENT));
        singleTicket.addPassenger(Passenger(STUDENT));
        singleTicket.addPassenger(Passenger(PENSIONER));

        BOOST_REQUIRE_CLOSE(singleTicket.calculateCost(), 382.64, 0.01);
    }

    BOOST_AUTO_TEST_CASE(SingleTicketCostCase2)
    {
        Train train("IC 10133", 500, "Gdansk", "Warszawa", TrainType(TrainType::Type::REGULAR));

        ptime pt1(date(2020,Jan,6), time_duration(22,20,0));
        ptime pt2(date(2020,Jan,7),time_duration(2,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        SingleTicket singleTicket(false, &train, Ticket::PaymentMethod::ONLINE, ldt1, ldt2);
        singleTicket.addPassenger(Passenger(CHILD));
        singleTicket.addPassenger(Passenger(CHILD));
        singleTicket.addPassenger(Passenger(REGULAR));
        singleTicket.addPassenger(Passenger(REGULAR));

        BOOST_REQUIRE_CLOSE(singleTicket.calculateCost(), 840, 0.01);
    }

    BOOST_AUTO_TEST_CASE(SingleTicketCostCase3)
    {
        Train train("IC 10233", 30, "Gdansk", "Gdynia", TrainType(TrainType::Type::REGIONAL));

        ptime pt1(date(2020,Jan,16), time_duration(22,20,0));
        ptime pt2(date(2020,Jan,17),time_duration(22,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        SingleTicket singleTicket(true, &train, Ticket::PaymentMethod::CASH, ldt1, ldt2);
        singleTicket.addPassenger(Passenger(VETERAN));
        singleTicket.addPassenger(Passenger(PENSIONER));

        BOOST_REQUIRE_CLOSE(singleTicket.calculateCost(), 19.5, 0.01);
    }

    BOOST_AUTO_TEST_CASE(MonthlyTicketTrainErrorCase)
    {
        ptime pt(date(2020,Jan,4), hours(10));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt(pt, zone);
        try
        {
            MonthlyTicket monthlyTicket(false, nullptr, Ticket::PaymentMethod::CASH, ldt, Passenger(PassengerType::REGULAR));
        }
        catch(const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, "Train is nullptr");
        }
    }

    BOOST_AUTO_TEST_CASE(SingleTicketTrainErrorCase)
    {
        ptime pt1(date(2020,Jan,16), time_duration(22,20,0));
        ptime pt2(date(2020,Jan,17),time_duration(22,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);
        try
        {
            SingleTicket singleTicket(true, nullptr, Ticket::PaymentMethod::CASH, ldt1, ldt2);
        }
        catch(const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, "Train is nullptr");
        }
    }

    BOOST_AUTO_TEST_CASE(SingleTicketTimeErrorCase)
    {
        Train train("IC 10233", 30, "Gdansk", "Gdynia", TrainType(TrainType::Type::REGIONAL));

        ptime pt1(date(2020,Jan,17), time_duration(22,20,0));
        ptime pt2(date(2020,Jan,16),time_duration(22,30,0));
        time_zone_ptr zone(new posix_time_zone("UTC+01"));
        local_date_time ldt1(pt1, zone);
        local_date_time ldt2(pt2, zone);

        try
        {
            SingleTicket singleTicket(true, &train, Ticket::PaymentMethod::CASH, ldt1, ldt2);
        }
        catch(const char* msg)
        {
            BOOST_REQUIRE_EQUAL(msg, "Arrival time less than departure time");
        }
    }

BOOST_AUTO_TEST_SUITE_END()