#include "functors/TicketsSearchByTrainFunc.h"
#include "exceptions/TicketException.h"
#include "exceptions/TrainException.h"

TicketsSearchByTrainFunc::TicketsSearchByTrainFunc(const Train* trainToSearchFor)
: trainToSearchFor(trainToSearchFor)
{
	if(trainToSearchFor == nullptr)
		throw TrainException().trainIsNullptr;
}

bool TicketsSearchByTrainFunc::operator()(const TicketPtr& ticket)
{
	return trainToSearchFor == ticket->getTrain();
}

void TicketsSearchByTrainFunc::addFound(const void* ticket)
{
	if(ticket == nullptr)
		throw TicketException().ticketIsNullptr;

	foundTickets.push_back((const Ticket*)(ticket));
}

const std::vector<const Ticket*>& TicketsSearchByTrainFunc::getFoundTickets()
{
	return foundTickets;
}

TicketsSearchByTrainFunc::~TicketsSearchByTrainFunc()
{

}

int TicketsSearchByTrainFunc::foundAmount()
{
	return foundTickets.size();
}
