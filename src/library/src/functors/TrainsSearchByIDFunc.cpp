#include "functors/TrainsSearchByIDFunc.h"
#include "exceptions/TrainException.h"

TrainsSearchByIDFunc::TrainsSearchByIDFunc(const std::string& IDToSearchFor)
: IDToSearchFor(IDToSearchFor), foundTrain(nullptr)
{
	if(IDToSearchFor == "")
		throw TrainException().IDIsEmpty;
}

TrainsSearchByIDFunc::~TrainsSearchByIDFunc()
{

}

bool TrainsSearchByIDFunc::operator()(const TrainPtr& train)
{
	return IDToSearchFor == train->getId();
}

void TrainsSearchByIDFunc::addFound(const void* train)
{
	if(train == nullptr)
		throw TrainException().trainIsNullptr;

	foundTrain = (const Train*)train;
}

int TrainsSearchByIDFunc::foundAmount()
{
	return 1;
}

const Train* TrainsSearchByIDFunc::getFoundTrain()
{
	return foundTrain;
}
