//
// Created by student on 29.12.2019.
//

#include "ticket/Passenger.h"

Passenger::Passenger(PassengerType passengerType) : passengerType(passengerType)
{

}

Passenger::~Passenger()
{

}

float Passenger::getDiscount()
{
	float discount = 0.0f;

    switch(passengerType)
	{
		case REGULAR:	discount = 0.0f; break;
		case STUDENT: 	discount = 0.51f; break;
		case CHILD:		discount = 0.5f; break;
		case PENSIONER:	discount = 0.6f; break;
		case VETERAN:	discount = 0.75f; break;
	}

	return discount;
}

PassengerType Passenger::getPassengerType() const
{
    return passengerType;
}
