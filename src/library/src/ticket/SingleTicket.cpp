//
// Created by student on 29.12.2019.
//

#include "ticket/SingleTicket.h"
#include "exceptions/SingleTicketException.h"

using namespace std;
using namespace boost;
using namespace local_time;

SingleTicket::SingleTicket(bool isFirstClassWagon, const Train* train, PaymentMethod paymentMethod,
		const local_date_time& departureTime, const local_date_time& arrivalTime)
		try : Ticket(isFirstClassWagon, train, paymentMethod), departureTime(departureTime), arrivalTime(arrivalTime)
{
    if(arrivalTime < departureTime)
        throw SingleTicketException().arrivalTimeLessThanDepartureTime;
}
catch (const char* msg)
{
	throw;
}

SingleTicket::~SingleTicket()
{

}

const local_date_time &SingleTicket::getDepartureTime() const
{
    return departureTime;
}

const local_date_time &SingleTicket::getArrivalTime() const
{
    return arrivalTime;
}

unsigned int SingleTicket::getNumberOfPassengers() const
{
	return passengers.size();
}

float SingleTicket::calculateCost() const
{
	float baseCost = Ticket::calculateCost();
    float cost = 0.0f;

    for(Passenger passenger : passengers)
	{
    	cost += baseCost - baseCost * passenger.getDiscount();
	}

    return cost;
}

string SingleTicket::toString() const
{
    return Ticket::toString()
        + "\nDeparture time: " + lexical_cast<string>(getDepartureTime())
        + "\nArrival time: " + lexical_cast<string>(getArrivalTime());
}

void SingleTicket::addPassenger(Passenger passenger)
{
	passengers.push_back(passenger);
}