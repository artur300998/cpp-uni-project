#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "ticket/Ticket.h"
#include "train/Train.h"
#include "exceptions/TicketException.h"
#include "exceptions/TrainException.h"

using namespace std;
using namespace boost;
using namespace uuids;

Ticket::Ticket(bool isFirstClassWagon, const Train* train, PaymentMethod paymentMethod)
        : ID(random_generator()()), isFirstClassWagon(isFirstClassWagon), train(train), paymentMethod(paymentMethod)
{
    if(train == nullptr)
        throw TrainException().trainIsNullptr;
}

Ticket::~Ticket()
{

}

float Ticket::calculateCost() const
{
	float cost = train->distanceCost();

	if(isFirstClassWagon)
		cost *= 1.5f;

    return cost;
}

string Ticket::toString() const
{
    string text = "ID: " + lexical_cast<string>(ID);
    if(isFirstClassWagon)
        text += "\nFirst class";
    else
        text += "\nSecond class";
    text += "\nTrain info: " + train->toString()
            + "\nPayment method: ";
    if(paymentMethod == ONLINE)
        text += "Online";
    else
        text += "Cash";
    text += "\nCost: " + to_string(calculateCost());
    return text;
}

const uuid &Ticket::getId() const
{
    return ID;
}

bool Ticket::getIsFirstClassWagon() const
{
    return isFirstClassWagon;
}

const Train* Ticket::getTrain() const
{
    return train;
}

Ticket::PaymentMethod Ticket::getPaymentMethod() const
{
    return paymentMethod;
}

bool Ticket::operator==(const Ticket& ticket) const
{
	return this->ID == ticket.ID;
}
