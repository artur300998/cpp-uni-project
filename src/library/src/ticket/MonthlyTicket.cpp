//
// Created by student on 29.12.2019.
//

#include "ticket/MonthlyTicket.h"

using namespace std;
using namespace boost;
using namespace local_time;

MonthlyTicket::MonthlyTicket(bool isFirstClassWagon, const Train* train, PaymentMethod paymentMethod,
		const local_date_time& validFrom, Passenger passenger)
		: Ticket(isFirstClassWagon, train, paymentMethod), validFrom(validFrom), passenger(passenger) {}

MonthlyTicket::~MonthlyTicket()
{

}

const local_date_time &MonthlyTicket::getValidFrom() const
{
    return validFrom;
}

unsigned int MonthlyTicket::getNumberOfPassengers() const
{
	return 1;
}

float MonthlyTicket::calculateCost() const
{
    return Ticket::calculateCost() * 10.0f;
}

string MonthlyTicket::toString() const
{
    return Ticket::toString()
        + "\nValid from: " + lexical_cast<string>(getValidFrom());
}

void MonthlyTicket::addPassenger(Passenger passenger)
{
	this->passenger = passenger;
}
