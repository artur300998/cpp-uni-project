//
// Created by student on 03.01.2020.
//

#include "repositories/TrainsRepository.h"
#include "exceptions/TrainsRepositoryException.h"

using namespace std;

TrainsRepository::TrainsRepository()
{

}

TrainsRepository::TrainsRepository(const vector<TrainPtr> &trains) : trains(trains)
{

}

TrainsRepository::~TrainsRepository()
{

}

void TrainsRepository::create(TrainPtr& train)
{
	vector<TrainPtr>::iterator it =
			find_if(trains.begin(), trains.end(), [train](TrainPtr& trainPtr)->bool{
				return *train == *(trainPtr.get());
			});

	if(it != trains.end())
		throw TrainsRepositoryException().trainAlreadyExists;

	trains.push_back(train);
}

void TrainsRepository::remove(const TrainPtr& train)
{
	vector<TrainPtr>::iterator it =
			find_if(trains.begin(), trains.end(), [train](TrainPtr& trainPtr)->bool{
				return *train == *(trainPtr.get());
			});

	if(it == trains.end())
		throw TrainsRepositoryException().trainNotFoundOnRemove;

	trains.erase(it);
}

const vector<TrainPtr>* TrainsRepository::getAll() const
{
	return &trains;
}

void TrainsRepository::find(SearchFunc<TrainPtr>& searchFunc)
{
	auto it = find_if(trains.begin(), trains.end(), ref(searchFunc));
	if(it != trains.end())
	{
		searchFunc.addFound(it->get());
	}
}
