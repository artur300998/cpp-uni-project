//
// Created by student on 03.01.2020.
//

#include "train/Train.h"
#include "repositories/TicketsRepository.h"
#include "exceptions/TicketsRepositoryException.h"

using namespace std;

TicketsRepository::TicketsRepository()
{

}

TicketsRepository::TicketsRepository(const vector<TicketPtr> &tickets) : tickets(tickets)
{

}

TicketsRepository::~TicketsRepository()
{

}

void TicketsRepository::create(TicketPtr& ticket)
{
	tickets.push_back(ticket);
}

void TicketsRepository::remove(const TicketPtr& ticket)
{
	vector<TicketPtr>::iterator it =
			find_if(tickets.begin(), tickets.end(), [ticket](TicketPtr& ticketPtr)->bool{
				return *ticket == *(ticketPtr.get());
			});

	if(it == tickets.end())
		throw TicketsRepositoryException().ticketNotFoundOnRemove;

	tickets.erase(it);
}

const vector<TicketPtr>* TicketsRepository::getAll() const
{
	return &tickets;
}

void TicketsRepository::find(SearchFunc<TicketPtr>& searchFunc)
{
	auto it = find_if(tickets.begin(), tickets.end(), ref(searchFunc));
	while(it != tickets.end())
	{
		searchFunc.addFound(it->get());
		it = find_if(it + 1, tickets.end(), ref(searchFunc));
	}
}
