//
// Created by student on 03.01.2020.
//

#include "functors/TicketsSearchByTrainFunc.h"
#include "managers/TicketsManager.h"
#include "train/Train.h"
#include "exceptions/TicketsManagerException.h"
#include "exceptions/TrainException.h"

using namespace std;

TicketsManager::TicketsManager()
{

}

TicketsManager::~TicketsManager()
{

}

void TicketsManager::addTicket(TicketPtr& ticket)
{
	try
	{
		const_cast<Train*>(ticket->getTrain())->occupySeats(ticket->getNumberOfPassengers());
	}
	catch (const char* msg)
	{
		throw;
	}

	ticketsRepository.create(ticket);
}

void TicketsManager::removeTicket(const TicketPtr& ticket)
{
	try
	{
		ticketsRepository.remove(ticket);
	}
	catch (const char* msg)
	{
		throw TicketsManagerException().ticketDoesntExist;
	}

	const_cast<Train*>(ticket->getTrain())->freeSeats(ticket->getNumberOfPassengers());
}

int TicketsManager::getTicketsByTrain(const Train* train, vector<const Ticket*>& tickets)
{
	TicketsSearchByTrainFunc ticketsSearchByTrainFunc(train);
	ticketsRepository.find(ticketsSearchByTrainFunc);

	tickets.clear();
	tickets = vector<const Ticket*>(ticketsSearchByTrainFunc.getFoundTickets());

	return ticketsSearchByTrainFunc.foundAmount();
}
