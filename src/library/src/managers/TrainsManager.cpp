//
// Created by student on 03.01.2020.
//

#include "managers/TrainsManager.h"
#include "functors/TrainsSearchByIDFunc.h"
#include "exceptions/TrainsManagerException.h"


TrainsManager::TrainsManager()
{

}

TrainsManager::~TrainsManager()
{

}

void TrainsManager::addTrain(TrainPtr& train)
{
	try
	{
		trainsRepository.create(train);
	}
	catch (const char* msg)
	{
		throw TrainsManagerException().trainAlreadyExists;
	}
}

void TrainsManager::removeTrain(const TrainPtr& train)
{
	try
	{
		trainsRepository.remove(train);
	}
	catch (const char* msg)
	{
		throw TrainsManagerException().trainDoesntExist;
	}
}

const Train* TrainsManager::getTrainByID(const std::string& id)
{
	TrainsSearchByIDFunc trainsSearchByIdFunc(id);
	trainsRepository.find(trainsSearchByIdFunc);

	return trainsSearchByIdFunc.getFoundTrain();
}
