//
// Created by student on 29.12.2019.
//

#include "train/Train.h"
#include "exceptions/TrainException.h"

using namespace std;

Train::Train(const std::string& id, unsigned int distance, const std::string& departureStation,
			 const std::string& arrivalStation, TrainType trainType, unsigned int capacity)
      : ID(id), distance(distance), departureStation(departureStation), arrivalStation(arrivalStation),
      	trainType(trainType), capacity(capacity), occupiedSeats(0)
{
    if(ID.empty())
        throw TrainException().IDIsEmpty;

    if(distance == 0)
        throw TrainException().distanceIsZero;
}

Train::~Train()
{

}

const string &Train::getId() const
{
    return ID;
}

unsigned int Train::getDistance() const
{
    return distance;
}

const string &Train::getDepartureStation() const
{
    return departureStation;
}

const string &Train::getArrivalStation() const
{
    return arrivalStation;
}

const TrainType& Train::getTrainType() const
{
    return trainType;
}

float Train::distanceCost() const
{
    return trainType.distanceCost(distance);
}

string Train::toString() const
{
    string text = getTrainType().toString()
            + "\nID: " + getId()
            + "\nDeparture station: " + getDepartureStation()
            + "\nArrival station: " + getArrivalStation()
            + "\nDistance: " + to_string(getDistance());
    return text;
}

bool Train::operator==(const Train& train) const
{
	return this->ID == train.ID;
}

unsigned int Train::getCapacity() const
{
	return capacity;
}

unsigned int Train::getOccupiedSeats() const
{
	return occupiedSeats;
}

void Train::occupySeats(int amount)
{
	if(occupiedSeats + amount  > capacity)
		throw TrainException().noSeatsAvailable;

	occupiedSeats += amount;
}

void Train::freeSeats(int amount)
{
	if((int)occupiedSeats < amount)
		throw TrainException().cantFreeSeats;

	occupiedSeats -= amount;
}
