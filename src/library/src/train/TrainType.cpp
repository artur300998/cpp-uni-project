//
// Created by student on 29.12.2019.
//

#include "train/TrainType.h"

TrainType::TrainType(TrainType::Type type) : type(type)
{
	switch(type)
	{
		case REGIONAL: 		distanceCostFun = &TrainType::regionalTrainDistanceCost; break;
		case HIGH_SPEED: 	distanceCostFun = &TrainType::highSpeedTrainDistanceCost; break;
		case REGULAR: 		distanceCostFun = &TrainType::regularTrainDistanceCost; break;
	}
}

TrainType::~TrainType()
{

}

float TrainType::regionalTrainDistanceCost(unsigned int distance) const
{
    return 5 + distance * 0.5f;
}

float TrainType::highSpeedTrainDistanceCost(unsigned int distance) const
{
    return 200 - 5000.0f / (distance + 30);
}

float TrainType::regularTrainDistanceCost(unsigned int distance) const
{
    return 30 + (distance * distance) / 1000.0f;
}

float TrainType::distanceCost(unsigned int distance) const
{
	return (this->*distanceCostFun)(distance);
}

std::string TrainType::toString() const
{
    switch(type)
    {
        case REGIONAL: 		return "Regional train";
        case HIGH_SPEED: 	return "High speed train";
        case REGULAR: 		return "Regular train";
    }
    return "";
}